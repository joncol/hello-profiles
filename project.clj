(defproject hello-profiles "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.10.1"]]
  :target-path "target/%s"
  ;; To be able to quickly run each main using `lein`.
  :aliases {"run-promise-getter" ["with-profile" "promise-getter" "run"]
            "run-message-getter" ["with-profile" "message-getter" "run"]
            "run-promise-updater" ["with-profile" "promise-updater" "run"]}
  :profiles {:promise-getter
             {:main ^:skip-aot hello-profiles.promise-getter.main}

             :message-getter
             {:main ^:skip-aot hello-profiles.message-getter.main}

             :promise-updater
             {:main ^:skip-aot hello-profiles.promise-updater.main}

             :uberjar {:aot :all}})
