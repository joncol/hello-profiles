# hello-profiles

This is a test of using multiple mains from a single (Leiningen) project.

## The main functions

The 3 different main functions are in the namepaces:

- `hello-profiles.promise-getter.main`
- `hello-profiles.message-getter.main`
- `hello-profiles.promise-updater.main`

## Running via `lein`

Aliases have been created to facilitate running via `lein`. Use one of the
following:

- `lein run-promise-getter`
- `lein run-message-getter`
- `lein run-promise-updater`

## Running via uberjar

First build the uberjar by running `lein uberjar`.

Then you can start a specific by main by issuing one of:
- `java -cp target/uberjar/hello-profiles-0.1.0-SNAPSHOT-standalone.jar clojure.main -m hello-profiles.promise-getter.main`
- `java -cp target/uberjar/hello-profiles-0.1.0-SNAPSHOT-standalone.jar clojure.main -m hello-profiles.message-getter.main`
- `java -cp target/uberjar/hello-profiles-0.1.0-SNAPSHOT-standalone.jar clojure.main -m hello-profiles.promise-updater.main`
